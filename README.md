# Offene Verwaltung
Ressourcen zur Nutzung von Freier und Open-Source-Software in der öffentlichen Verwaltung.

## Warum Freie Software / Open Source?
Die öffentliche Verwaltung ist in ihrem Handeln an den Grundsatz der Wirtschaftlichkeit gebunden.
Bei der Entwicklung von Software konkurrieren Behörden i.d.R. nicht untereinandern, sondern können von ihren Bemühungen gegenseitig profitieren.
Freie und Open-Source-Softwarelizenzen bilden einen standardisierten Rahmen für eine gemeinsame, wirtschaftliche Nutzung und Entwicklung von Software durch Behörden, Zivilgesellschaft und Industrie.
Durch den Erwerb und Einsatz von Freier und Open-Source-Software reduziert die Verwaltung Abhängigkeiten zu einzelnen Unternehmen (sog. „Vendor Lock-In“).
Behörden können sich durch Veröffentlichung von oder Mitarbeit an Freier und Open-Source-Software einen guten Ruf erarbeiten, was sich nicht zuletzt auch positiv auf die Bestrebungen zur Gewinnung von talentierten Fachkräften auswirken kann.

- Initiative [Public Money Public Code](https://publiccode.eu/) der FSFE
  - [Expertenbroschüre zur Modernisierung der öffentlichen digitalen Infrastruktur durch öffentlichen Code](https://fsfe.org/activities/publiccode/brochure.de.html) mit vielen Hintergründen und Argumentationshilfen für Freie Software in der Verwaltung
- [Stellungnahme des CCC Darmstadt zu Freier Software in der Verwaltung mit Fokus auf Kommunen](https://www.chaos-darmstadt.de/2020/stellungnahme-zum-kommunalbericht-2019/)- [Open Source in Kommunen – Ein Baustein für mehr Digitale Souveränität](https://www.kgst.de/doc/20210706A0006): Bericht der Kommunalen Gemeinschaftsstelle für Verwaltungsmanagement (KGSt)
  - [Ankündigung von Do-FOSS dazu](https://blog.do-foss.de/beitrag/open-source-in-kommunen-ein-baustein-fuer-mehr-digitale-souveraenitaet/)
- [Strategische Marktanalyse zur Reduzierung von Abhängigkeiten von einzelnen Software-Anbietern](https://www.cio.bund.de/SharedDocs/Publikationen/DE/Aktuelles/20190919_strategische_marktanalyse.pdf?__blob=publicationFile) ("Micrsoft-Studie") des BMI
- Dokumentation [Software-Rebellen - Die Macht des Teilens](https://www.youtube.com/watch?v=SRXdilrbujM) (ARTE)
  - [Filmtipp von Do-FOSS dazu](https://blog.do-foss.de/kolumne/software-rebellen-die-macht-des-teilens/)
- Dokumentation [Das Microsoft-Dilemma - Europa als Softwarekolonie](https://tube.tchncs.de/w/fda999b4-1e21-4325-997a-889a60d0d480)
  - [Beschreibung auf programm.ard.de](https://programm.ard.de/TV/daserste/das-microsoft-dilemma/eid_28106504116395)
- [Basistipps zur IT-Sicherheit: Open Source Software](https://www.bsi.bund.de/DE/Themen/Verbraucherinnen-und-Verbraucher/Informationen-und-Empfehlungen/Cyber-Sicherheitsempfehlungen/Updates-Browser-Open-Source-Software/Open-Source-Vorabversionen-von-Betriebssystemen/open-source-vorabversionen-von-betriebssystemen_node.html) des BSI
- [Vortrag: Ohne Open Source? Wie hätte das denn gehen sollen?](https://programm.froscon.org/2023/events/2977.html) von [Bianca Kastl](https://programm.froscon.org/2023/speakers/1861.html) auf der [FrOSCon 2023](https://froscon.org/) - [[Slides](https://bkastl.de/froscon23/)]

## Open-Source-Strategieen / Politische Entscheidungen
### Überblick
- [Sachstand zum Einsatz von Open-Source-Software in EU-Mitgliedsstaaten](https://www.bundestag.de/resource/blob/944586/edbfcd79cb197d945da98fdae691fef8/WD-10-042-22-pdf-data.pdf) der Wissenschaftliche Dienste des Deutschen Bundestages vom 13. März 2023
- [Interactive Resource Map](https://joinup.ec.europa.eu/collection/open-source-observatory-osor/interactive-resource-map) des Open Source Observatory (OSOR) der Europäischen Kommission
- [Vortrag: FLOSS von und in der öffentlichen Verwaltung - Eine kleine Zwischenbilanz](https://programm.froscon.org/2023/events/2952.html) von [Marco](https://programm.froscon.org/2023/speakers/1844.html) auf der [FrOSCon 2023](https://froscon.org/) - [[Slides](https://programm.froscon.org/2023/system/event_attachments/attachments/000/000/747/original/FLOSS-von-und-in-der-OEV.pdf)]
- [Vortrag: Freie Software für Deutschlands Verwaltungen? - Rätsel um den „Souveränen Arbeitsplatz“](https://programm.froscon.org/2023/events/2886.html) von [Johannes Näder (fsfe)](https://programm.froscon.org/2023/speakers/1786.html) auf der [FrOSCon 2023](https://froscon.org/) - [[Slides](https://programm.froscon.org/2023/system/event_attachments/attachments/000/000/771/original/FrOSCon_2023_Johannes_N%C3%A4der_Souver%C3%A4ner_Arbeitsplatz.pdf)]
- [Vortrag: IT is a public good: ZenDiS als Bindeglied zwischen öffentlicher Verwaltung und Open Source Ökosystem](https://programm.froscon.org/2023/events/2917.html) von Andreas Reckert-Lodde (BMI / ZenDiS) auf der [FrOSCon 2023](https://froscon.org/)

### Europäische Kommission
- [Open-Source-Strategie der Europäischen Kommission](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_de)
  - [Commission Decision of 8 December 2021 on the open source licensing and reuse of Commission software 2021/C 495 I/01 ("Software Reuse
Decision")](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32021D1209(01))
  - [Open-Source-Software-Strategie 2020-2023: Wie die EU-Kommission an der eigenen digitalen Transformation arbeitet](https://netzpolitik.org/2020/wie-die-eu-kommission-an-der-eigenen-digitalen-transformation-arbeitet/) - Artikel von netzpolitik.org vom 22.10.2020

### IT-Planungsrat
- [Strategische Architekturrichtlinie SR8 „Einsatz von Open Source“](https://docs.fitko.de/arc/policies/foederale-it-architekturrichtlinien#sr8): _Der Quellcode aus der Realisierung digitaler Angebote der Verwaltung (Eigenentwicklung) ist als Open Source, d. h. in nachnutzbarer Form zur Verfügung zu stellen. Open Source Lösungen sind Nicht-Open Source Lösungen vorzuziehen, sofern geeignet und wirtschaftlich._
- [Übersicht über weitere (teilweise schwächere) Vorgaben im Kontext der OZG-Umsetzung](https://github.com/codedust/awesome-egov-de/#policies)

### Bundesministerium des Innern und für Heimat (BMI)
- [13. Prinzip des Servicestandards für die OZG-Umsetzung: Open Source](https://www.onlinezugangsgesetz.de/Webs/OZG/DE/grundlagen/servicestandard/prinzip-13/prinzip-13-node.html): _Der Quellcode (oder Quellcodes) aus der Realisierung digitaler Angebote der Verwaltung (Eigenentwicklung) wird als Open Source, das heißt in nachnutzbarer Form mit kostenfreier, eine Veränderung gestattender Lizenzierung, zur Verfügung gestellt._


## Praktische Hinweise zur Föderung/Nutzung/Veröffentlichung von Open-Source in der Öffentlichen Verwaltung
### Allgemeines Informationsmaterial
- [Leitfaden für die Migration von Software](https://fragdenstaat.de/dokumente/234078-migration-software/) des CIO Bund
  - [Ankündigung dazu](https://www.cio.bund.de/SharedDocs/Kurzmeldungen/DE/2012/20120305_migrationsleitfaden_4_0.html)
  - [Artikel vom ifrOSS dazu](https://www.ifross.org/?q=artikel/migrationsleitfaden-40-fokussiert-offene-standards)
  - Begleitdokument [Rechtliche Aspekte der Nutzung, Verbreitung und Weiterentwicklung von Open-Source-Software](https://nbn-resolving.org/urn:nbn:de:bund-ibib-bmi-31376)
  - Begleitdokument [Wirtschaftliche Aspekte von Software-Migrationen - Begleitdokument zum Migrationsleitfaden 4.0](https://nbn-resolving.org/urn:nbn:de:bund-ibib-bmi-31387)
- [Praxis-Leitfaden Open Source Software in der Schweizer Bundesverwaltung](https://www.bk.admin.ch/dam/bk/de/dokumente/dti/ikt-vorgaben/strategien/oss/Praxis-Leitfaden_OSS_Bundesverwaltung_V_1-0.pdf.download.pdf/Praxis-Leitfaden_OSS_Bundesverwaltung_V_1-0.pdf)
- [Nutzung von Open-Source-Software im DLR](https://www.dlr.de/tm/PortalData/43/Resources/dokumente/tm_dokumente/OpenSource-Software_DLR_2022.pdf)
- [Nachhaltiger Open Source Einsatz für die digital souveräne Verwaltung](https://osb-alliance.de/news/publikationen/veroeffentlichungen/nachhaltiger-open-source-einsatz-fuer-die-digital-souveraene-verwaltung-paper): Hinweise und Erläuterungen der OSB Alliance zum Einsatz von Open-Source-Software
- [Machbarkeitsnachweise zu alternativen IT-Lösungen des BMI](https://web.archive.org/web/20220116024817/https://cio.bund.de/SharedDocs/Kurzmeldungen/DE/2020/20200330_Machbarkeitsnachweise_download.pdf?__blob=publicationFile) (Übersicht Machbarkeitsnachweise auf Seite 8, Steckbriefe ab Seite 13)

### Technik
- [publiccode.yml](https://github.com/publiccodeyml/publiccode.yml): Metadaten-Standard zur Auszeichnung und Auffindbarkeit von Softwareprojekten der Öffentlichen Verwaltung
  - Wird u.a. von der OpenCoDE-Plattform [genutzt](https://wikijs.opencode.de/de/Hilfestellungen_und_Richtlinien/Leitlinien#h-2-aufnahme-ins-softwareverzeichnis).

### Vorlagen für Policies / Vorgaben / Richtlinien
- [Knowledge Center](https://joinup.ec.europa.eu/collection/open-source-observatory-osor/knowledge-centre) des Open Source Observatory (OSOR) der EU Kommission
- [Guidelines on Commission software distribution](https://code.europa.eu/info/about/-/blob/master/guidelines/code-europa-eu-guidelines.md) der EU-Kommission
- [Empfehlungen für die Veröffentlichung von Open Source-Projekten bei BR Data](https://github.com/br-data/open-source-guidelines): Gute Zusammenfassung sinnvoller Vorgehensweisen beim Veröffentlichen von Code als Open Source
- [Open Source Policy - Examples and best practices ](https://ghinda.com/blog/opensource/2020/open-source-policy-examples-and-best-practices.html)
- [Übersicht über Open Source Policies und Vorlagen auf GitHub](https://github.com/todogroup/policies)
- [Open Source Contribution Policy der DB Systel GmbH](https://github.com/dbsystel/open-source-policies/blob/master/Open-Source-Contribution-Policy.md)

### Beschaffung / Rechtliches
- [Handreichung zur Nutzung der EVB-IT beim Einsatz von Open Source Software](https://osb-alliance.de/publikationen/veroeffentlichungen/handreichungen-zur-nutzung-der-evb-it-beim-einsatz-von-open-source-software) der Open Source Business Alliance
- [eGovernment-Podcast zu innovativer Beschaffung in der Verwaltung](https://egovernment-podcast.com/egov071-innovative-beschaffung/)
- [FAQ des Institut für Rechtsfragen der Freien und Open Source Software (ifrOSS)](https://www.ifross.org/?q=faq-haeufig-gestellte-fragen)
- [Contributor License Agreement Chooser](https://contributoragreements.org/ca-cla-chooser/)

###	Formulierungsvorschläge für vertraglich vereinbarte Nutzungsrechte
> Haftungsausschluss: Für die Nutzung der hier dargestellten Formulierungsvorschläge wird keinerlei Haftung übernommen.

#### Nutzungsrechte an den Arbeitsergebnissen
> Quelle (mit leichten Anpassungen): https://fragdenstaat.de/anfrage/rahmenvertrage-init-aktiengesellschaft-fur-digitale-kommunikation/519485/anhang/rahmenvertrag-weiterentwicklung-gsb-auf-basis-von-opensource.pdf

Der jeweilige Leistungsempfänger erhält vom Auftragnehmer das ausschließliche, zeitlich, räumlich und inhaltlich unbeschränkte, insbesondere übertragbare Nutzungsrecht an sämtlichen Arbeitsergebnissen (auch an Zwischenergebnissen), unabhängig davon, ob diese in verkörperter oder unverkörperter Form vorliegen.
Der Leistungsempfänger soll in die Lage versetzt werden, die Arbeitsergebnisse zu jedem erdenklichen, bei Vertragsschluss bekannten sowie noch unbekannten Zweck zu nutzen.
Das Nutzungsrecht schließt die Berechtigung ein, die Arbeitsergebnisse an Dritte weiterzugeben und Unterlizensierungen vorzunehmen.

Die Nutzungsrechteeinräumung gilt auch hinsichtlich von Komponenten, die der Auftragnehmer nicht im Zusammenhang mit diesem Vertrag entwickelt hat und im Rahmen der Erfüllung dieses Vertrags einsetzt bzw. verwendet.
Abweichendes muss ausdrücklich schriftlich vereinbart werden.

Der Auftragnehmer gewährleistet, dass dem vereinbarten Nutzungsrechteumfang keine Rechte Dritter entgegenstehen und die vertragsgemäße Nutzung der Leistung durch den Leistungsempfänger nicht in Patente, Lizenzen oder sonstige Schutzrechte Dritter eingreift.
Insbesondere gewährt der Auftragnehmer, dass er mit allen Mitarbeiter:innen des Projektteams (einschließlich Unterauftragnehmern) ausreichende urheberrechtliche Vereinbarungen getroffen hat, um die Nutzungsrechte so, wie hier vereinbart, übertragen zu können.

Verwendet der Auftragnehmer für die Erbringung seiner Leistungen Rechte Dritter und sind diese auch für die Nutzung von Leistungen durch den jeweiligen Leistungsempfänger notwendig, wird der Auftragnehmer dem Leistungsempfänger an diesen Rechten ein räumlich, inhaltlich und zeitlich unbeschränktes, übertragbares und unterlizenzierbares Nutzungsrecht verschaffen.

Beim Einsatz von Open-Source-Software (OSS) gelten die vorgenannten Nutzungsrechtsregelungen nur insoweit, als dies das OSS-Lizenzmodell zulässt.
Sofern ein OSS-Lizenzmodell einen Ausschluss von Haftung und Gewährleistung insgesamt vorsieht, haftet der Auftragnehmer für diese wie für eigene Leistungen.

Die Einräumung der Nutzungsrechte ist durch den Tagessatz mit abgegolten.

#### Veröffentlichung unter der Open-Source-Lizenz für die Europäische Union (EUPL)
Die entwickelte Software sowie alle damit verbundenen Artefakte sollen unter einer Open-Source-Lizenz bereitgestellt werden.
Wenn nicht anders vereinbart, verpflichtet sich der Auftragnehmer, alle im Rahmen dieser Leistungsbeschreibung erstellten Software-Werke unter der EUPL zur Verfügung zu stellen.
Der Auftragnehmer gewährleistet, dass er das Urheberrecht am Originalwerk innehat oder dieses an ihn lizenziert wurde und dass er befugt ist, diese Lizenz zu erteilen.
Der Auftragnehmer verpflichtet sich bei Selbstnutzungen der Werke zur Einhaltung der erteilten Softwarelizenz.
Insbesondere schließt dies die Erteilung von konkurrierenden, zur EUPL inkompatiblen Lizenzen aus.

Die in der Open-Source-Lizenz für die Europäische Union beschriebenen Haftungs- und Gewährleistungsausschlüsse finden nur auf das Rechtsverhältnis zum Rechteinhaber Anwendung, jedoch nicht auf das Vertragsverhältnis vom Aufraggeber zum Auftragnehmer.

#### Besondere Bestimmungen bei Nutzung von EVB-IT-Verträgen
> Quelle: [Handreichung zur Nutzung der EVB-IT beim Einsatz von Open Source Software](https://osb-alliance.de/publikationen/veroeffentlichungen/handreichungen-zur-nutzung-der-evb-it-beim-einsatz-von-open-source-software) der Open Source Business Alliance
> Anhänge als Textdateien: https://git.fitko.de/Marco_Holz/formulierungsvorschlaege-evb-it

Kommen im Rahmen der Vertragsgestaltung EVB-IT-Vertragstypen zum Einsatz, so gelten abweichend von den Regelungen im jeweiligen EVB-IT-Vertrag, in den dazugehörigen AGB, sowie in den beiliegenden Mustern (z.B. Leistungsnachweise) die in den folgenden Anhängen aufgeführten Regelungen zum jeweiligen EVB-IT-Vertragstyp:
- Anhang „Regelungen für Open Source Software zum EVB-IT Erstellungsvertrag“
- Anhang „Regelungen für Open Source Software zum EVB-IT Systemvertrag“
- Anhang „Regelungen für Open Source Software zum EVB-IT Überlassungsvertrag Typ A (Konstellation A)“
- Anhang „Regelungen für Open Source Software zum EVB-IT Überlassungsvertrag Typ A (Konstellation B)“
- Anhang „Regelungen für Open Source Software zum EVB-IT Dienstvertrag“
- Anhang „Regelungen für Open Source Software zum EVB-IT Pflegevertrag S“

Im Übrigen sind bei der Nutzung von EVB-IT-Verträgen die im Anhang „Handreichung zur Nutzung der EVB-IT beim Einsatz von Open Source Software“ aufgeführten „weiteren Hinweise“ zum Ausfüllen des Vertragsmusters anzuwenden.

Darüber hinaus erteilt der Auftragnehmer dem Leistungsempfänger für die Gültigkeitsdauer der am Originalwerk bestehenden Urheberrechte eine weltweite, unentgeltliche, ausschließliche, unterlizenzierbare Lizenz, die den Leistungsempfänger berechtigt:
- das Werk uneingeschränkt zu nutzen,
- das Werk zu vervielfältigen,
- das Werk zu verändern und Bearbeitungen auf der Grundlage des Werks zu schaffen,
- das Werk öffentlich zugänglich zu machen, was das Recht einschließt, das Werk oder Vervielfältigungsstücke davon öffentlich bereitzustellen oder wahrnehmbar zu machen oder das Werk, soweit möglich, öffentlich aufzuführen,
- das Werk oder Vervielfältigungen davon zu verbreiten,
- das Werk oder Vervielfältigungen davon zu vermieten oder zu verleihen,
- das Werk oder Vervielfältigungen davon weiter zu lizenzieren.

Für die Wahrnehmung dieser Rechte können beliebige, derzeit bekannte oder künftige Medien, Träger und Formate verwendet werden, soweit das geltende Recht dem nicht entgegensteht.
Für die Länder, in denen Urheberpersönlichkeitsrechte an dem Werk bestehen, verzichtet der Auftragnehmer im gesetzlich zulässigen Umfang auf seine Urheberpersönlichkeitsrechte, um die Lizenzierung der oben aufgeführten Verwertungsrechte wirksam durchführen zu können.
Der Auftragnehmer erteilt dem Leistungsempfänger ein nicht ausschließliches, unentgeltliches Nutzungsrecht an seinen Patenten, sofern dies zur Ausübung der erteilten Rechte am Werk notwendig ist.

Der Auftragnehmer gewährleistet, dass er das Urheberrecht am Originalwerk innehat oder dieses an ihn lizenziert wurde und dass er befugt ist, diese Lizenz zu erteilen.

Der Auftragnehmer überträgt dem Leistungsempfänger sämtliche im Rahmen dieses Vertrages und seiner Erfüllung entstandenen, Leistungsschutz- und sonstigen Schutzrechte.
Der Leistungsempfänger ist berechtigt, die ihm übertragenen Rechte an Dritte zu deren freier und uneingeschränkter Verwendung weiterzugeben.
Die Urheberpersönlichkeitsrechte des Auftragnehmers bleiben unberührt.

#### Besondere Bestimmungen zu Konzepten, Dokumentationen, Handbüchern und Konfigurationsdateien
Die entstandenen Konzepte, Dokumentationen und Handbücher sind unter der Lizenz Creative Commons Attribution Share Alike 4.0 (CC BY-SA 4.0, siehe https://creativecommons.org/licenses/by-sa/4.0/) zu lizenzieren.
Neu erstellte Konfigurationsdateien für Drittkomponenten, die aufgrund der geringen Schöpfungshöhe keinen urheberrechtlichen Schutz genießen, sind mit der Public Domain Dedication CC0 1.0 Universell (CC0 1.0, siehe https://creativecommons.org/publicdomain/zero/1.0/deed.de) zu kennzeichnen.

#### Bereitstellung und Übergabe der entwickelten Software und Schnittstellenspezifikationen
Die entwickelten Software-Artefakte und (Schnittstellen-)Spezifikationen sowie alle damit verbundenen Artefakte sind in einer Git-basierten Versionsverwaltung zu pflegen.
Zum Zweck der Übergabe stellt der Auftraggeber eine GitLab-Instanz bereit.
Die jeweils aktuelle Fassung der von dieser Leistungsbeschreibung abgedeckten Lieferergebnisse ist dem Leistungsempfänger jederzeit auf Verlangen, spätestens aber zur Projektabnahme im Quellcode unter Wahrung der vereinbarten Nutzungsrechte bereitzustellen.

#### Bereitstellung und Übergabe der entwickelten Konzepte, Dokumentationen und weiterer Dokumente
Die zu erstellenden Dokumente sind auf Anfrage unverzüglich in ihrer jeweils aktuellen Fassung an den Leistungsempfänger zu übergeben und im Projektverlauf kontinuierlich weiterzuentwickeln.
Die Dokumente müssen zu jedem, mit entsprechendem Vorlauf verabredeten, Übergabezeitpunkt und zum Zeitpunkt der Übergabe bei Abschluss der Leistungserbringung zwingend den jeweils aktuellen Projektstatus abbilden.
Die Dokumente sind in der Textauszeichnungssprache Markdown zu verfassen und in einer Git-basierten Versionsverwaltung zu pflegen.
Zu diesem Zweck stellt der Auftraggeber eine GitLab-Instanz bereit.
Abweichungen von dieser Regel sind im Einzelfall nach schriftlicher Absprache mit dem Leistungsempfänger möglich.

## Softwarelizenzen
Freie Softwarelizenzen stellen die rechtsichere Nutzung von Freier Software sicher.
Sie dienen als Vereinbarung zw. den Urheber:innen einer Software und deren Nutzer:innen.
Grundsätzlich wird unterschieden zw. sog. [freizügigen (permissiven) Softwarelizenzen](https://de.wikipedia.org/wiki/Freiz%C3%BCgige_Open-Source-Lizenz), die eine Weiterverbreitung unter beliebigen (auch prorietären, nicht-offenen) Lizenzen, erlauben und [Copyleft](https://de.wikipedia.org/wiki/Copyleft)-Lizenzen, die eine Verbreitung ausschließlich unter den Bedingungen der vorgegebenen Lizenz erlauben.

- [Gute Einführung zum Thema Freie-Software-Lizenzen vom Prototype Fund](https://prototypefund.de/freie-open-source-lizenzen/)
- [Lizenzübersicht der EU (Joinup Licensing Assistant)](https://joinup.ec.europa.eu/collection/eupl/solution/joinup-licensing-assistant/jla-find-and-compare-software-licenses)
- [ifrOSS Lizenzcenter](https://ifross.github.io/ifrOSS/Lizenzcenter)

### European Union Public Licence (EUPL)
Die [EUPL](https://de.wikipedia.org/wiki/European_Union_Public_Licence) ist eine von der Europäischen Kommission entwickelte Copyleft-Lizenz für Freie/Open-Source-Software.
Copyleft bedeutet dabei, dass unter EUPL-lizenzierte Software dauerhaft nur unter der EUPL-Lizenz weiterverbreitet werden darf und von Dritten nicht ohne Weiteres aufgegriffen und unter einer proprietären (nicht-Open-Source) Lizenz verkauft werden darf (eine Verbreitung unter einer kompatiblen Lizenz nach Artikel 5 der EUPL ist im Rahmen der sog. Kompatibilitäts-Klausel möglich).

Sie ist kompatible mit gängigen Open-Source-Software-Lizenzen (inkl. GPL), liegt in 23 Sprachen der EU vor, ist OSI-zertifiziert und wird von der Free Software Foundation als freie Softwarelizenz angesehen.
Die EUPL ist aufgrund der besonders hohen Rechtssicherheit (liegt in deutscher Sprache vor, wurde speziell mit dem Ziel der Konsistens zum Urheberrecht der 27 Mitgliedsstaaten der EU entwickelt) besonders geeignet für die Nutzung durch Behörden.

Durch die Möglichkeit, EUPL-Code durch Zusammenfühung mit anderen Projekten in andere Copyleft-Lizenzen umzulizenzieren, ist das Copyleft der EUPL schwächer als z.B das Copyleft der [GPL](https://www.gnu.org/licenses/gpl.html).

Die EUPL enthält eine Server-/Cloud-Klausel (wie die [AGPL](https://gnu.org/licenses/agpl.html)), die den Lizenznehmer auch bei einer Bereitstellung der Software als Cloud-Dienst (Software-as-a-Service) zur Bereitstellung des Quellcodes verpflichtet.

- [Dokumentation (Guidelines, Documents FAQs)](https://joinup.ec.europa.eu/node/702120) auf Joinup
- [Lizenztext der EUPL](https://eur-lex.europa.eu/legal-content/DE/TXT/?uri=CELEX:32017D0863)
- [ifross: "EU-Kommission veröffentlicht neue EUPL-Version"](https://www.ifross.org/?q=artikel/eu-kommission-ver-ffentlicht-neue-eupl-version)
- [Vergleich der EUPL mit der GPLv3](https://joinup.ec.europa.eu/collection/eupl/news/eupl-or-gplv3-comparison-t)
- Artikel [“Copyleft” or "Reciprocal"… The meaning of “Copyleft” in EUPL](https://joinup.ec.europa.eu/node/140379)
- Artikel [Why the EUPL instead of MIT? - Permissive or Share-alike licence?](https://interoperable-europe.ec.europa.eu/node/726383)

#### Beispiele für unter der EUPL lizenzierte Software
siehe auch: [Wikipedia: Anwendungsbeispiele der EUPL](https://de.wikipedia.org/wiki/European_Union_Public_Licence#Anwendungsbeispiele)

- [DVDV-Bibliothek](https://www.itzbund.de/DE/itloesungen/standardloesungen/dvdv/downloads/downloads.html) zur Nutzung des Deutschen Verwaltungsdiensteverzeichnisses
- [AusweisApp2](https://www.ausweisapp.bund.de)
- [FIT-Connect](https://www.fitko.de/projektmanagement/fit-connect)

## Beispiele für die erfolgreiche Nutzung/Veröffentlichung von FOSS in der ÖV
- [okfde/awesome-behoerden-floss](https://github.com/okfde/awesome-behoerden-floss): Kuratierte Liste von F/LOSS-Software, die erfolgreich in der Verwaltung eingesetzt wird mit Kontakten zu Menschen, die sie einsetzen
- [Liste von IFG-Anfragen zu Freier Software in der öffentlichen Verwaltung](https://wiki.fsfe.org/Activities/PMPC/de)
- [Pressemitteilung: Nextcloud, Open-Xchange und Univention kündigen Software-Suite für den öffentlichen Sektor an](https://nextcloud.com/press/pr20210129/#german)
- [Corona-Warn-App](https://github.com/corona-warn-app)
- [Berlin Open Source](https://berlinopensource.de/): Überblick über Open-Source-Projekte, die von öffentlichen Verwaltungen im Land Berlin entwickelt, beauftragt oder gefördert werden

siehe auch: [Übersicht über Code-Repositories der öffentlichen Verwaltung](https://github.com/codedust/awesome-egov-de/#code-repositories)

## Relevante Kontakte in Deutschland
siehe auch: [Auflistung von Organisationen, die Freie Software unterstüzen](https://wiki.fsfe.org/Activities/List_of_organisations_supporting_Free_Software) der FSFE

- [Free Software Foundation Europe](https://fsfe.org/)
  - [Mailingliste zum Thema Freie Software in Kommunen](https://lists.fsfe.org/mailman/listinfo/fs-kommunen)
- [Institut für Rechtsfragen der Freien und Open Source Software](https://www.ifross.org/)
- [Team der OpenCoDE-Plattform](https://wikijs.opencode.de/de/Kontakt)
- [Code for Germany](https://codefor.de/)
- [Open Source Business Aliance - OSB Alliance](https://osb-alliance.de/)
- [Do-FOSS, Initiative für den Einsatz Freier und Quelloffener Software bei der Stadt Dortmund ](https://do-foss.de/)

## Andere Informationssammlungen zum Thema
- [Interaktive Europakarte des Open Source Observatory (OSOR) der EU-Kommission](https://joinup.ec.europa.eu/collection/open-source-observatory-osor/news/interactive-map-oss-resources)
- [Linksammlung auf berlinopensource.de](https://berlinopensource.de/links/)
- [aweseome-egov-de](https://github.com/codedust/awesome-egov-de/): A curated list of resources on egovernment in Germany

